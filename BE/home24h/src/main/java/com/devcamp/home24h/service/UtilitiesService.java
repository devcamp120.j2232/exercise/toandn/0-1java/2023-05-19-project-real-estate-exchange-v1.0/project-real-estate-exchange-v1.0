package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.Utilities;
import com.devcamp.home24h.repository.IUtilitiesRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class UtilitiesService {
  @Autowired
  IUtilitiesRepository gIUtilitiesRepository;

  public ArrayList<Utilities> getAllUtilities() {
    ArrayList<Utilities> listUtilities = new ArrayList<>();
    gIUtilitiesRepository.findAll().forEach(listUtilities::add);
    return listUtilities;
  }

  public Utilities createUtilities(Utilities pUtilities) {
    try {
      Utilities vUtilitiesSave = gIUtilitiesRepository.save(pUtilities);
      return vUtilitiesSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Utilities updateUtilities(Utilities pUtilities, Optional<Utilities> pUtilitiesData) {
    try {
      Utilities vUtilities = pUtilitiesData.get();
      vUtilities.setDescription(pUtilities.getDescription());
      vUtilities.setName(pUtilities.getName());
      vUtilities.setPhoto(pUtilities.getPhoto());
      Utilities vUtilitiesSave = gIUtilitiesRepository.save(vUtilities);
      return vUtilitiesSave;
    } catch (Exception e) {
      return null;
    }
  }
}
