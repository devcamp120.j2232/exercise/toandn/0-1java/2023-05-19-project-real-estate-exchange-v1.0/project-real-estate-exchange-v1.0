package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/constructionContractor")
public class ConstructionContractorController {
  @Autowired
  IConstructionContractorRepository gIConstructionContractorRepository;
  @Autowired
  ConstructionContractorService gConstructionContractorService;

  @GetMapping("/all")
  public ResponseEntity<List<ConstructionContractor>> getAllConstructionContractor() {
    try {
      return new ResponseEntity<>(gConstructionContractorService.getAllConstructionContractor(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/details/{id}")
  public ResponseEntity<Object> getConstructionContractorById(@PathVariable Integer id) {
    Optional<ConstructionContractor> vConstructionContractorData = gIConstructionContractorRepository.findById(id);
    if (vConstructionContractorData.isPresent()) {
      try {
        ConstructionContractor vConstructionContractor = vConstructionContractorData.get();
        return new ResponseEntity<>(vConstructionContractor, HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      ConstructionContractor vConstructionContractorNull = new ConstructionContractor();
      return new ResponseEntity<>(vConstructionContractorNull, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createConstructionContractor(
      @Valid @RequestBody ConstructionContractor paramConstructionContractor) {
    try {
      return new ResponseEntity<>(
          gConstructionContractorService.createConstructionContractor(paramConstructionContractor), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified ConstructionContractor: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateConstructionContractor(@PathVariable Integer id,
      @Valid @RequestBody ConstructionContractor paramConstructionContractor) {
    Optional<ConstructionContractor> vConstructionContractorData = gIConstructionContractorRepository.findById(id);
    if (vConstructionContractorData.isPresent()) {
      try {
        return new ResponseEntity<>(
            gConstructionContractorService.updateConstructionContractor(paramConstructionContractor,
                vConstructionContractorData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified ConstructionContractor: " + e.getCause().getCause().getMessage());
      }
    } else {
      ConstructionContractor vConstructionContractorNull = new ConstructionContractor();
      return new ResponseEntity<>(vConstructionContractorNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<ConstructionContractor> vConstructionContractorData = gIConstructionContractorRepository.findById(id);
    if (vConstructionContractorData.isPresent()) {
      try {
        gIConstructionContractorRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      ConstructionContractor vConstructionContractorNull = new ConstructionContractor();
      return new ResponseEntity<>(vConstructionContractorNull, HttpStatus.NOT_FOUND);
    }
  }
}
