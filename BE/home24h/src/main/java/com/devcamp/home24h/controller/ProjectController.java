package com.devcamp.home24h.controller;

import java.util.*;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/project")
public class ProjectController {
  @Autowired
  IProjectRepository gIProjectRepository;
  @Autowired
  IStreetRepository gIStreetRepository;
  @Autowired
  IWardRepository gIWardRepository;
  @Autowired
  IDistrictRepository gIDistrictRepository;
  @Autowired
  IProvinceRepository gIProvinceRepository;
  @Autowired
  ProjectService gProjectService;

  @GetMapping("/all")
  public ResponseEntity<List<Project>> getAllProject() {
    try {
      return new ResponseEntity<>(gProjectService.getAllProject(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{StreetId}/{WardId}/{DistrictId}/{ProvinceId}")
  public ResponseEntity<Object> createProject(@PathVariable Integer StreetId, @PathVariable Integer WardId,
      @PathVariable Integer DistrictId, @PathVariable Integer ProvinceId,
      @Valid @RequestBody Project paramProject) {
    Optional<Street> vStreetData = gIStreetRepository.findById(StreetId);
    Optional<Ward> vWardData = gIWardRepository.findById(WardId);
    Optional<District> vDistrictData = gIDistrictRepository.findById(DistrictId);
    Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
    if (vStreetData.isPresent() && vWardData.isPresent() && vDistrictData.isPresent() && vProvinceData.isPresent()) {
      try {
        return new ResponseEntity<>(
            gProjectService.createProject(paramProject, vStreetData, vWardData, vDistrictData, vProvinceData),
            HttpStatus.CREATED);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Create specified Project: " + e.getCause().getCause().getMessage());
      }
    } else {
      District vDistrictNull = new District();
      return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/update/{id}/{StreetId}/{WardId}/{DistrictId}/{ProvinceId}")
  public ResponseEntity<Object> updateProject(@PathVariable Integer StreetId, @PathVariable Integer WardId,
      @PathVariable Integer id, @PathVariable Integer DistrictId,
      @PathVariable Integer ProvinceId,
      @Valid @RequestBody Project paramProject) {
    Optional<Project> vProjectData = gIProjectRepository.findById(id);
    if (vProjectData.isPresent()) {
      Optional<Street> vStreetData = gIStreetRepository.findById(StreetId);
      Optional<Ward> vWardData = gIWardRepository.findById(WardId);
      Optional<District> vDistrictData = gIDistrictRepository.findById(DistrictId);
      Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
      if (vStreetData.isPresent() && vWardData.isPresent() && vDistrictData.isPresent() && vProvinceData.isPresent()) {
        try {
          return new ResponseEntity<>(
              gProjectService.updateProject(paramProject, vProjectData, vStreetData, vWardData, vDistrictData,
                  vProvinceData),
              HttpStatus.OK);
        } catch (Exception e) {
          return ResponseEntity.unprocessableEntity()
              .body("Failed to Update specified Project: " + e.getCause().getCause().getMessage());
        }
      } else {
        District vDistrictNull = new District();
        return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
      }
    } else {
      Project vProjectNull = new Project();
      return new ResponseEntity<>(vProjectNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProjectById(@PathVariable Integer id) {
    Optional<Project> vProjectData = gIProjectRepository.findById(id);
    if (vProjectData.isPresent()) {
      try {
        gIProjectRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Project vProjectNull = new Project();
      return new ResponseEntity<>(vProjectNull, HttpStatus.NOT_FOUND);
    }
  }
}
