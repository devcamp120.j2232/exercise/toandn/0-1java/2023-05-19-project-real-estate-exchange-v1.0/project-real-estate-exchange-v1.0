package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.DesignUnit;
import com.devcamp.home24h.repository.IDesignUnitRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class DesignUnitService {
  @Autowired
  IDesignUnitRepository gIDesignUnitRepository;

  public ArrayList<DesignUnit> getAllDesignUnit() {
    ArrayList<DesignUnit> listDesignUnit = new ArrayList<>();
    gIDesignUnitRepository.findAll().forEach(listDesignUnit::add);
    return listDesignUnit;
  }

  public DesignUnit createDesignUnit(DesignUnit pDesignUnit) {
    try {
      DesignUnit vDesignUnitSave = gIDesignUnitRepository.save(pDesignUnit);
      return vDesignUnitSave;
    } catch (Exception e) {
      return null;
    }
  }

  public DesignUnit updateDesignUnit(DesignUnit pDesignUnit, Optional<DesignUnit> pDesignUnitData) {
    try {
      DesignUnit vDesignUnit = pDesignUnitData.get();
      vDesignUnit.setName(pDesignUnit.getName());
      vDesignUnit.setAddress(pDesignUnit.getAddress());
      vDesignUnit.setDescription(pDesignUnit.getDescription());
      vDesignUnit.setEmail(pDesignUnit.getEmail());
      vDesignUnit.setFax(pDesignUnit.getFax());
      vDesignUnit.setNote(pDesignUnit.getNote());
      vDesignUnit.setPhone(pDesignUnit.getPhone());
      vDesignUnit.setPhone2(pDesignUnit.getPhone2());
      vDesignUnit.setProjects(pDesignUnit.getProjects());
      vDesignUnit.setWebsite(pDesignUnit.getWebsite());
      DesignUnit vDesignUnitSave = gIDesignUnitRepository.save(vDesignUnit);
      return vDesignUnitSave;
    } catch (Exception e) {
      return null;
    }
  }
}
