package com.devcamp.home24h.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "master_layout")
public class MasterLayout {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "description")
  private String description;

  @Column(name = "acreage")
  private Double acreage;

  @Column(name = "apartment_list")
  private String apartmentList;

  @Column(name = "photo")
  private String photo;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_create", nullable = false)
  private Date dateCreate;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false)
  private Date dateUpdate;

  @ManyToOne
  @JsonIgnore
  private Project project;

  public MasterLayout() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Double getAcreage() {
    return acreage;
  }

  public void setAcreage(Double acreage) {
    this.acreage = acreage;
  }

  public String getApartmentList() {
    return apartmentList;
  }

  public void setApartmentList(String apartmentList) {
    this.apartmentList = apartmentList;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  public Date getDateCreate() {
    return dateCreate;
  }

  public void setDateCreate(Date dateCreate) {
    this.dateCreate = dateCreate;
  }

  public Date getDateUpdate() {
    return dateUpdate;
  }

  public void setDateUpdate(Date dateUpdate) {
    this.dateUpdate = dateUpdate;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

}
