package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class RealeStateService {
  @Autowired
  IRealeStateRepository gIRealeStateRepository;

  public ArrayList<RealeState> getAllRealeState() {
    ArrayList<RealeState> listRealeState = new ArrayList<>();
    gIRealeStateRepository.findAll().forEach(listRealeState::add);
    return listRealeState;
  }

  public RealeState createRealeState(RealeState pRealeState,
      Optional<Street> pStreetData, Optional<Ward> pWardData, Optional<District> pDistrictData,
      Optional<Province> pProvinceData, Optional<Project> pProjectData,
      Optional<Customer> pCustomerData) {
    try {
      RealeState vRealeState = new RealeState();
      vRealeState.setAcreage(pRealeState.getAcreage());
      vRealeState.setAddress(pRealeState.getAddress());
      vRealeState.setAdjacentAlleyMinWidth(pRealeState.getAdjacentAlleyMinWidth());
      vRealeState.setAdjacentFacadeNum(pRealeState.getAdjacentFacadeNum());
      vRealeState.setAdjacentRoad(pRealeState.getAdjacentRoad());
      vRealeState.setDescription(pRealeState.getDescription());
      vRealeState.setApartCode(pRealeState.getApartCode());
      vRealeState.setApartType(pRealeState.getApartType());
      vRealeState.setLat(pRealeState.getLat());
      vRealeState.setLng(pRealeState.getLng());
      vRealeState.setApartLoca(pRealeState.getApartLoca());
      vRealeState.setBalcony(pRealeState.getBalcony());
      vRealeState.setBath(pRealeState.getBath());
      vRealeState.setBedroom(pRealeState.getBedroom());
      vRealeState.setCLCL(pRealeState.getCLCL());
      vRealeState.setCTXDPrice(pRealeState.getCTXDPrice());
      vRealeState.setCTXDValue(pRealeState.getCTXDValue());
      vRealeState.setCreateBy(pRealeState.getCreateBy());
      vRealeState.setDTSXD(pRealeState.getDTSXD());
      vRealeState.setDateCreate(pRealeState.getDateCreate());
      vRealeState.setDirection(pRealeState.getDirection());
      vRealeState.setDistance2facade(pRealeState.getDistance2facade());
      vRealeState.setFactor(pRealeState.getFactor());
      vRealeState.setFsbo(pRealeState.getFsbo());
      vRealeState.setFurnitureType(pRealeState.getFurnitureType());
      vRealeState.setLandscapeView(pRealeState.getLandscapeView());
      vRealeState.setLegalDoc(pRealeState.getLegalDoc());
      vRealeState.setLongX(pRealeState.getLongX());
      vRealeState.setNumberFloors(pRealeState.getNumberFloors());
      vRealeState.setPhoto(pRealeState.getPhoto());
      vRealeState.setPrice(pRealeState.getPrice());
      vRealeState.setPriceMin(pRealeState.getPriceMin());
      vRealeState.setPriceRent(pRealeState.getPriceRent());
      vRealeState.setPriceTime(pRealeState.getPriceTime());
      vRealeState.setRequest(pRealeState.getRequest());
      vRealeState.setReturnRate(pRealeState.getReturnRate());
      vRealeState.setShape(pRealeState.getShape());
      vRealeState.setStreetHouse(pRealeState.getStreetHouse());
      vRealeState.setStructure(pRealeState.getStructure());
      vRealeState.setTitle(pRealeState.getTitle());
      vRealeState.setTotalFloors(pRealeState.getTotalFloors());
      vRealeState.setType(pRealeState.getType());
      vRealeState.setUpdateBy(pRealeState.getUpdateBy());
      vRealeState.setViewNum(pRealeState.getViewNum());
      vRealeState.setWallArea(pRealeState.getWallArea());
      vRealeState.setWidthY(pRealeState.getWidthY());

      vRealeState.setStreet(pStreetData.get());
      vRealeState.setWard(pWardData.get());
      vRealeState.setProvince(pProvinceData.get());
      vRealeState.setDistrict(pDistrictData.get());
      vRealeState.setProject(pProjectData.get());
      vRealeState.setCustomer(pCustomerData.get());
      RealeState vRealeStateSave = gIRealeStateRepository.save(vRealeState);
      return vRealeStateSave;
    } catch (Exception e) {
      return null;
    }
  }

  public RealeState updateRealeState(RealeState pRealeState, Optional<RealeState> pRealeStateData,
      Optional<Street> pStreetData,
      Optional<Ward> pWardData,
      Optional<District> pDistrictData, Optional<Province> pProvinceData, Optional<Project> pProjectData,
      Optional<Customer> pCustomerData) {
    try {
      RealeState vRealeState = new RealeState();
      vRealeState.setAcreage(pRealeState.getAcreage());
      vRealeState.setAddress(pRealeState.getAddress());
      vRealeState.setAdjacentAlleyMinWidth(pRealeState.getAdjacentAlleyMinWidth());
      vRealeState.setAdjacentFacadeNum(pRealeState.getAdjacentFacadeNum());
      vRealeState.setAdjacentRoad(pRealeState.getAdjacentRoad());
      vRealeState.setDescription(pRealeState.getDescription());
      vRealeState.setApartCode(pRealeState.getApartCode());
      vRealeState.setApartType(pRealeState.getApartType());
      vRealeState.setLat(pRealeState.getLat());
      vRealeState.setLng(pRealeState.getLng());
      vRealeState.setApartLoca(pRealeState.getApartLoca());
      vRealeState.setBalcony(pRealeState.getBalcony());
      vRealeState.setBath(pRealeState.getBath());
      vRealeState.setBedroom(pRealeState.getBedroom());
      vRealeState.setCLCL(pRealeState.getCLCL());
      vRealeState.setCTXDPrice(pRealeState.getCTXDPrice());
      vRealeState.setCTXDValue(pRealeState.getCTXDValue());
      vRealeState.setCreateBy(pRealeState.getCreateBy());
      vRealeState.setDTSXD(pRealeState.getDTSXD());
      vRealeState.setDateCreate(pRealeState.getDateCreate());
      vRealeState.setDirection(pRealeState.getDirection());
      vRealeState.setDistance2facade(pRealeState.getDistance2facade());
      vRealeState.setFactor(pRealeState.getFactor());
      vRealeState.setFsbo(pRealeState.getFsbo());
      vRealeState.setFurnitureType(pRealeState.getFurnitureType());
      vRealeState.setLandscapeView(pRealeState.getLandscapeView());
      vRealeState.setLegalDoc(pRealeState.getLegalDoc());
      vRealeState.setLongX(pRealeState.getLongX());
      vRealeState.setNumberFloors(pRealeState.getNumberFloors());
      vRealeState.setPhoto(pRealeState.getPhoto());
      vRealeState.setPrice(pRealeState.getPrice());
      vRealeState.setPriceMin(pRealeState.getPriceMin());
      vRealeState.setPriceRent(pRealeState.getPriceRent());
      vRealeState.setPriceTime(pRealeState.getPriceTime());
      vRealeState.setRequest(pRealeState.getRequest());
      vRealeState.setReturnRate(pRealeState.getReturnRate());
      vRealeState.setShape(pRealeState.getShape());
      vRealeState.setStreetHouse(pRealeState.getStreetHouse());
      vRealeState.setStructure(pRealeState.getStructure());
      vRealeState.setTitle(pRealeState.getTitle());
      vRealeState.setTotalFloors(pRealeState.getTotalFloors());
      vRealeState.setType(pRealeState.getType());
      vRealeState.setUpdateBy(pRealeState.getUpdateBy());
      vRealeState.setViewNum(pRealeState.getViewNum());
      vRealeState.setWallArea(pRealeState.getWallArea());
      vRealeState.setWidthY(pRealeState.getWidthY());

      vRealeState.setStreet(pStreetData.get());
      vRealeState.setWard(pWardData.get());
      vRealeState.setProvince(pProvinceData.get());
      vRealeState.setDistrict(pDistrictData.get());
      vRealeState.setProject(pProjectData.get());
      vRealeState.setCustomer(pCustomerData.get());
      RealeState vRealeStateSave = gIRealeStateRepository.save(vRealeState);
      return vRealeStateSave;
    } catch (Exception e) {
      return null;
    }
  }
}
