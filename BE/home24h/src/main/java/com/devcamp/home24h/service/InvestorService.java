package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.Investor;
import com.devcamp.home24h.repository.IInvestorRepositoy;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class InvestorService {
  @Autowired
  IInvestorRepositoy gIInvestorRepository;

  public ArrayList<Investor> getAllInvestor() {
    ArrayList<Investor> listInvestor = new ArrayList<>();
    gIInvestorRepository.findAll().forEach(listInvestor::add);
    return listInvestor;
  }

  public Investor createInvestor(Investor pInvestor) {
    try {
      Investor vInvestorSave = gIInvestorRepository.save(pInvestor);
      return vInvestorSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Investor updateInvestor(Investor pInvestor, Optional<Investor> pInvestorData) {
    try {
      Investor vInvestor = pInvestorData.get();
      vInvestor.setAddress(pInvestor.getAddress());
      vInvestor.setDescription(pInvestor.getDescription());
      vInvestor.setEmail(pInvestor.getEmail());
      vInvestor.setFax(pInvestor.getFax());
      vInvestor.setName(pInvestor.getName());
      vInvestor.setNote(pInvestor.getNote());
      vInvestor.setPhone(pInvestor.getPhone());
      vInvestor.setPhone2(pInvestor.getPhone2());
      vInvestor.setProjects(pInvestor.getProjects());
      vInvestor.setWebsite(pInvestor.getWebsite());
      Investor vInvestorSave = gIInvestorRepository.save(vInvestor);
      return vInvestorSave;
    } catch (Exception e) {
      return null;
    }
  }
}
