package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/utilities")
public class UtilitiesController {
  @Autowired
  IUtilitiesRepository gIUtilitiesRepository;
  @Autowired
  UtilitiesService gUtilitiesService;

  @GetMapping("/all")
  public ResponseEntity<List<Utilities>> getAllUtilities() {
    try {
      return new ResponseEntity<>(gUtilitiesService.getAllUtilities(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/details/{id}")
  public ResponseEntity<Object> getUtilitiesById(@PathVariable Integer id) {
    Optional<Utilities> vUtilitiesData = gIUtilitiesRepository.findById(id);
    if (vUtilitiesData.isPresent()) {
      try {
        Utilities vUtilities = vUtilitiesData.get();
        return new ResponseEntity<>(vUtilities, HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Utilities vUtilitiesNull = new Utilities();
      return new ResponseEntity<>(vUtilitiesNull, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createUtilities(@Valid @RequestBody Utilities paramUtilities) {
    try {
      return new ResponseEntity<>(gUtilitiesService.createUtilities(paramUtilities), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Utilities: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateUtilities(@PathVariable Integer id,
      @Valid @RequestBody Utilities paramUtilities) {
    Optional<Utilities> vUtilitiesData = gIUtilitiesRepository.findById(id);
    if (vUtilitiesData.isPresent()) {
      try {
        return new ResponseEntity<>(gUtilitiesService.updateUtilities(paramUtilities, vUtilitiesData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Utilities: " + e.getCause().getCause().getMessage());
      }
    } else {
      Utilities vUtilitiesNull = new Utilities();
      return new ResponseEntity<>(vUtilitiesNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<Utilities> vUtilitiesData = gIUtilitiesRepository.findById(id);
    if (vUtilitiesData.isPresent()) {
      try {
        gIUtilitiesRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Utilities vUtilitiesNull = new Utilities();
      return new ResponseEntity<>(vUtilitiesNull, HttpStatus.NOT_FOUND);
    }
  }
}
