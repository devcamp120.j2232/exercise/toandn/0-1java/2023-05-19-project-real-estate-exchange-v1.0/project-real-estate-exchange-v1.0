package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/investor")
public class InvestorController {
  @Autowired
  IInvestorRepositoy gIInvestorRepository;
  @Autowired
  InvestorService gInvestorService;

  @GetMapping("/all")
  public ResponseEntity<List<Investor>> getAllInvestor() {
    try {
      return new ResponseEntity<>(gInvestorService.getAllInvestor(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/details/{id}")
  public ResponseEntity<Object> getInvestorById(@PathVariable Integer id) {
    Optional<Investor> vInvestorData = gIInvestorRepository.findById(id);
    if (vInvestorData.isPresent()) {
      try {
        Investor vInvestor = vInvestorData.get();
        return new ResponseEntity<>(vInvestor, HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Investor vInvestorNull = new Investor();
      return new ResponseEntity<>(vInvestorNull, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createInvestor(@Valid @RequestBody Investor paramInvestor) {
    try {
      return new ResponseEntity<>(gInvestorService.createInvestor(paramInvestor), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Investor: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateInvestor(@PathVariable Integer id,
      @Valid @RequestBody Investor paramInvestor) {
    Optional<Investor> vInvestorData = gIInvestorRepository.findById(id);
    if (vInvestorData.isPresent()) {
      try {
        return new ResponseEntity<>(gInvestorService.updateInvestor(paramInvestor, vInvestorData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Investor: " + e.getCause().getCause().getMessage());
      }
    } else {
      Investor vInvestorNull = new Investor();
      return new ResponseEntity<>(vInvestorNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<Investor> vInvestorData = gIInvestorRepository.findById(id);
    if (vInvestorData.isPresent()) {
      try {
        gIInvestorRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Investor vInvestorNull = new Investor();
      return new ResponseEntity<>(vInvestorNull, HttpStatus.NOT_FOUND);
    }
  }
}
