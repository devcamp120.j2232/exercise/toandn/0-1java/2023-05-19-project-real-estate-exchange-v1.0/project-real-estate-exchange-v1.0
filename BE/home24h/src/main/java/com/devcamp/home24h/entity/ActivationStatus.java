package com.devcamp.home24h.entity;

public enum ActivationStatus {
  Y("Y"),
  N("N");

  private final String value;

  ActivationStatus(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
