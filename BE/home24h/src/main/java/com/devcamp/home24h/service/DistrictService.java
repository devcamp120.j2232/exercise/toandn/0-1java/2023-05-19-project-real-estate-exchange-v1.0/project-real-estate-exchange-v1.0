package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class DistrictService {
  @Autowired
  IDistrictRepository gIDistrictRepository;

  public ArrayList<District> getAllDistrict() {
    ArrayList<District> listDistrict = new ArrayList<>();
    gIDistrictRepository.findAll().forEach(listDistrict::add);
    return listDistrict;
  }

  public District createDistrict(District pDistrict,
      Optional<Province> pProvinceData) {
    try {
      District vDistrict = new District();
      vDistrict.setName(pDistrict.getName());
      vDistrict.setPrefix(pDistrict.getPrefix());
      vDistrict.setProjects(pDistrict.getProjects());
      vDistrict.setRealeStates(pDistrict.getRealeStates());
      vDistrict.setStreets(pDistrict.getStreets());
      vDistrict.setWards(pDistrict.getWards());
      vDistrict.setProvince(pProvinceData.get());
      District vDistrictSave = gIDistrictRepository.save(vDistrict);
      return vDistrictSave;
    } catch (Exception e) {
      return null;
    }
  }

  public District updateDistrict(District pDistrict, Optional<District> pDistrictData,
      Optional<Province> pProvinceData) {
    try {
      District vDistrict = pDistrictData.get();
      vDistrict.setName(pDistrict.getName());
      vDistrict.setPrefix(pDistrict.getPrefix());
      vDistrict.setProjects(pDistrict.getProjects());
      vDistrict.setRealeStates(pDistrict.getRealeStates());
      vDistrict.setStreets(pDistrict.getStreets());
      vDistrict.setWards(pDistrict.getWards());
      vDistrict.setProvince(pProvinceData.get());
      District vDistrictSave = gIDistrictRepository.save(vDistrict);
      return vDistrictSave;
    } catch (Exception e) {
      return null;
    }
  }
}
