package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/locations")
public class LocationsControlle {
  @Autowired
  ILocationsRepository gILocationsRepository;
  @Autowired
  LocationsService gLocationsService;

  @GetMapping("/all")
  public ResponseEntity<List<Locations>> getAllLocations() {
    try {
      return new ResponseEntity<>(gLocationsService.getAllLocations(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/details/{id}")
  public ResponseEntity<Object> getLocationsById(@PathVariable Integer id) {
    Optional<Locations> vLocationsData = gILocationsRepository.findById(id);
    if (vLocationsData.isPresent()) {
      try {
        Locations vLocations = vLocationsData.get();
        return new ResponseEntity<>(vLocations, HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Locations vLocationsNull = new Locations();
      return new ResponseEntity<>(vLocationsNull, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createLocations(@Valid @RequestBody Locations paramLocations) {
    try {
      return new ResponseEntity<>(gLocationsService.createLocations(paramLocations), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Locations: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateLocations(@PathVariable Integer id,
      @Valid @RequestBody Locations paramLocations) {
    Optional<Locations> vLocationsData = gILocationsRepository.findById(id);
    if (vLocationsData.isPresent()) {
      try {
        return new ResponseEntity<>(gLocationsService.updateLocations(paramLocations, vLocationsData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Locations: " + e.getCause().getCause().getMessage());
      }
    } else {
      Locations vLocationsNull = new Locations();
      return new ResponseEntity<>(vLocationsNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<Locations> vLocationsData = gILocationsRepository.findById(id);
    if (vLocationsData.isPresent()) {
      try {
        gILocationsRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Locations vLocationsNull = new Locations();
      return new ResponseEntity<>(vLocationsNull, HttpStatus.NOT_FOUND);
    }
  }
}
