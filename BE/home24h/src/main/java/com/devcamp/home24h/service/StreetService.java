package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class StreetService {
  @Autowired
  IStreetRepository gIStreetRepository;

  public ArrayList<Street> getAllStreet() {
    ArrayList<Street> listStreet = new ArrayList<>();
    gIStreetRepository.findAll().forEach(listStreet::add);
    return listStreet;
  }

  public Street createStreet(Street pStreet,
      Optional<District> pDistrictData, Optional<Province> pProvinceData) {
    try {
      Street vStreet = new Street();
      vStreet.setName(pStreet.getName());
      vStreet.setPrefix(pStreet.getPrefix());
      vStreet.setProvince(pProvinceData.get());
      vStreet.setDistrict(pDistrictData.get());
      Street vStreetSave = gIStreetRepository.save(vStreet);
      return vStreetSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Street updateStreet(Street pStreet, Optional<Street> pStreetData,
      Optional<District> pDistrictData, Optional<Province> pProvinceData) {
    try {
      Street vStreet = pStreetData.get();
      vStreet.setName(pStreet.getName());
      vStreet.setPrefix(pStreet.getPrefix());
      vStreet.setProvince(pProvinceData.get());
      vStreet.setDistrict(pDistrictData.get());
      Street vStreetSave = gIStreetRepository.save(vStreet);
      return vStreetSave;
    } catch (Exception e) {
      return null;
    }
  }
}
