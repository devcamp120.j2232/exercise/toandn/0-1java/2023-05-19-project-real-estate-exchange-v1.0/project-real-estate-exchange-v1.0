package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {
  @Autowired
  IProjectRepository gIProjectRepository;

  public ArrayList<Project> getAllProject() {
    ArrayList<Project> listProject = new ArrayList<>();
    gIProjectRepository.findAll().forEach(listProject::add);
    return listProject;
  }

  public Project createProject(Project pProject,
      Optional<Street> pStreetData, Optional<Ward> pWardData, Optional<District> pDistrictData,
      Optional<Province> pProvinceData) {
    try {
      Project vProject = new Project();
      vProject.setAcreage(pProject.getAcreage());
      vProject.setAddress(pProject.getAddress());
      vProject.setApartmentArea(pProject.getApartmentArea());
      vProject.setConstructArea(pProject.getConstructArea());
      vProject.setConstructionContractor(pProject.getConstructionContractor());
      vProject.setDescription(pProject.getDescription());
      vProject.setDesignUnit(pProject.getDesignUnit());
      vProject.setInvestor(pProject.getInvestor());
      vProject.setLat(pProject.getLat());
      vProject.setLng(pProject.getLng());
      vProject.setMasterLayouts(pProject.getMasterLayouts());
      vProject.setName(pProject.getName());
      vProject.setNumApartment(pProject.getNumApartment());
      vProject.setNumBlock(pProject.getNumBlock());
      vProject.setNumFloors(pProject.getNumFloors());
      vProject.setPhoto(pProject.getPhoto());
      vProject.setRegionLink(pProject.getRegionLink());
      vProject.setSlogan(pProject.getSlogan());
      vProject.setUtilities(pProject.getUtilities());
      vProject.setStreet(pStreetData.get());
      vProject.setWard(pWardData.get());
      vProject.setProvince(pProvinceData.get());
      vProject.setDistrict(pDistrictData.get());
      Project vProjectSave = gIProjectRepository.save(vProject);
      return vProjectSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Project updateProject(Project pProject, Optional<Project> pProjectData, Optional<Street> pStreetData,
      Optional<Ward> pWardData,
      Optional<District> pDistrictData, Optional<Province> pProvinceData) {
    try {
      Project vProject = new Project();
      vProject.setAcreage(pProject.getAcreage());
      vProject.setAddress(pProject.getAddress());
      vProject.setApartmentArea(pProject.getApartmentArea());
      vProject.setConstructArea(pProject.getConstructArea());
      vProject.setConstructionContractor(pProject.getConstructionContractor());
      vProject.setDescription(pProject.getDescription());
      vProject.setDesignUnit(pProject.getDesignUnit());
      vProject.setInvestor(pProject.getInvestor());
      vProject.setLat(pProject.getLat());
      vProject.setLng(pProject.getLng());
      vProject.setMasterLayouts(pProject.getMasterLayouts());
      vProject.setName(pProject.getName());
      vProject.setNumApartment(pProject.getNumApartment());
      vProject.setNumBlock(pProject.getNumBlock());
      vProject.setNumFloors(pProject.getNumFloors());
      vProject.setPhoto(pProject.getPhoto());
      vProject.setRegionLink(pProject.getRegionLink());
      vProject.setSlogan(pProject.getSlogan());
      vProject.setUtilities(pProject.getUtilities());
      vProject.setStreet(pStreetData.get());
      vProject.setWard(pWardData.get());
      vProject.setProvince(pProvinceData.get());
      vProject.setDistrict(pDistrictData.get());
      Project vProjectSave = gIProjectRepository.save(vProject);
      return vProjectSave;
    } catch (Exception e) {
      return null;
    }
  }
}
