package com.devcamp.home24h.controller;

import java.util.*;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/ward")
public class WardController {
  @Autowired
  IWardRepository gIWardRepository;
  @Autowired
  IDistrictRepository gIDistrictRepository;
  @Autowired
  IProvinceRepository gIProvinceRepository;
  @Autowired
  WardService gWardService;

  @GetMapping("/all")
  public ResponseEntity<List<Ward>> getAllWard() {
    try {
      return new ResponseEntity<>(gWardService.getAllWard(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{DistrictId}/{ProvinceId}")
  public ResponseEntity<Object> createWard(@PathVariable Integer DistrictId, @PathVariable Integer ProvinceId,
      @Valid @RequestBody Ward paramWard) {
    Optional<District> vDistrictData = gIDistrictRepository.findById(DistrictId);
    Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
    if (vDistrictData.isPresent() && vProvinceData.isPresent()) {
      try {
        return new ResponseEntity<>(gWardService.createWard(paramWard, vDistrictData, vProvinceData),
            HttpStatus.CREATED);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Create specified Ward: " + e.getCause().getCause().getMessage());
      }
    } else {
      District vDistrictNull = new District();
      return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/update/{id}/{DistrictId}/{ProvinceId}")
  public ResponseEntity<Object> updateWard(@PathVariable Integer id, @PathVariable Integer DistrictId,
      @PathVariable Integer ProvinceId,
      @Valid @RequestBody Ward paramWard) {
    Optional<Ward> vWardData = gIWardRepository.findById(id);
    if (vWardData.isPresent()) {
      Optional<District> vDistrictData = gIDistrictRepository.findById(DistrictId);
      Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
      if (vDistrictData.isPresent() && vProvinceData.isPresent()) {
        try {
          return new ResponseEntity<>(
              gWardService.updateWard(paramWard, vWardData, vDistrictData, vProvinceData),
              HttpStatus.OK);
        } catch (Exception e) {
          return ResponseEntity.unprocessableEntity()
              .body("Failed to Update specified Ward: " + e.getCause().getCause().getMessage());
        }
      } else {
        District vDistrictNull = new District();
        return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
      }
    } else {
      Ward vWardNull = new Ward();
      return new ResponseEntity<>(vWardNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteWardById(@PathVariable Integer id) {
    Optional<Ward> vWardData = gIWardRepository.findById(id);
    if (vWardData.isPresent()) {
      try {
        gIWardRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Ward vWardNull = new Ward();
      return new ResponseEntity<>(vWardNull, HttpStatus.NOT_FOUND);
    }
  }
}
