package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/Districts")
public class DistrictController {
  @Autowired
  IDistrictRepository gIDistrictRepository;
  @Autowired
  IProvinceRepository gIProvinceRepository;
  @Autowired
  DistrictService gDistrictService;

  @GetMapping("/all")
  public ResponseEntity<List<District>> getAllDistrict() {
    try {
      return new ResponseEntity<>(gDistrictService.getAllDistrict(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{ProvinceId}")
  public ResponseEntity<Object> createDistrict(@PathVariable Integer ProvinceId,
      @Valid @RequestBody District paramDistrict) {
    Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
    if (vProvinceData.isPresent()) {
      try {
        return new ResponseEntity<>(gDistrictService.createDistrict(paramDistrict, vProvinceData), HttpStatus.CREATED);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Create specified District: " + e.getCause().getCause().getMessage());
      }
    } else {
      Province vProvinceNull = new Province();
      return new ResponseEntity<>(vProvinceNull, HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/update/{id}/{ProvinceId}")
  public ResponseEntity<Object> updateDistrict(@PathVariable Integer id, @PathVariable Integer ProvinceId,
      @Valid @RequestBody District paramDistrict) {
    Optional<District> vDistrictData = gIDistrictRepository.findById(id);
    if (vDistrictData.isPresent()) {
      Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
      if (vProvinceData.isPresent()) {
        try {
          return new ResponseEntity<>(gDistrictService.updateDistrict(paramDistrict, vDistrictData, vProvinceData),
              HttpStatus.OK);
        } catch (Exception e) {
          return ResponseEntity.unprocessableEntity()
              .body("Failed to Update specified District: " + e.getCause().getCause().getMessage());
        }
      } else {
        Province vProvinceNull = new Province();
        return new ResponseEntity<>(vProvinceNull, HttpStatus.NOT_FOUND);
      }
    } else {
      District vDistrictNull = new District();
      return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteDistrictById(@PathVariable Integer id) {
    Optional<District> vDistrictData = gIDistrictRepository.findById(id);
    if (vDistrictData.isPresent()) {
      try {
        gIDistrictRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      District vDistrictNull = new District();
      return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
    }
  }
}
