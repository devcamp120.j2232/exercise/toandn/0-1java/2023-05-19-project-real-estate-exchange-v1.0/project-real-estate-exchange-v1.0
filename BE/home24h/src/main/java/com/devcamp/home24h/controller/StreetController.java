package com.devcamp.home24h.controller;

import java.util.*;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/street")
public class StreetController {
  @Autowired
  IStreetRepository gIStreetRepository;
  @Autowired
  IDistrictRepository gIDistrictRepository;
  @Autowired
  IProvinceRepository gIProvinceRepository;
  @Autowired
  StreetService gStreetService;

  @GetMapping("/all")
  public ResponseEntity<List<Street>> getAllStreet() {
    try {
      return new ResponseEntity<>(gStreetService.getAllStreet(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{DistrictId}/{ProvinceId}")
  public ResponseEntity<Object> createStreet(@PathVariable Integer DistrictId, @PathVariable Integer ProvinceId,
      @Valid @RequestBody Street paramStreet) {
    Optional<District> vDistrictData = gIDistrictRepository.findById(DistrictId);
    Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
    if (vDistrictData.isPresent() && vProvinceData.isPresent()) {
      try {
        return new ResponseEntity<>(gStreetService.createStreet(paramStreet, vDistrictData, vProvinceData),
            HttpStatus.CREATED);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Create specified Street: " + e.getCause().getCause().getMessage());
      }
    } else {
      District vDistrictNull = new District();
      return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/update/{id}/{DistrictId}/{ProvinceId}")
  public ResponseEntity<Object> updateStreet(@PathVariable Integer id, @PathVariable Integer DistrictId,
      @PathVariable Integer ProvinceId,
      @Valid @RequestBody Street paramStreet) {
    Optional<Street> vStreetData = gIStreetRepository.findById(id);
    if (vStreetData.isPresent()) {
      Optional<District> vDistrictData = gIDistrictRepository.findById(DistrictId);
      Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
      if (vDistrictData.isPresent() && vProvinceData.isPresent()) {
        try {
          return new ResponseEntity<>(
              gStreetService.updateStreet(paramStreet, vStreetData, vDistrictData, vProvinceData),
              HttpStatus.OK);
        } catch (Exception e) {
          return ResponseEntity.unprocessableEntity()
              .body("Failed to Update specified Street: " + e.getCause().getCause().getMessage());
        }
      } else {
        District vDistrictNull = new District();
        return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
      }
    } else {
      Street vStreetNull = new Street();
      return new ResponseEntity<>(vStreetNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteStreetById(@PathVariable Integer id) {
    Optional<Street> vStreetData = gIStreetRepository.findById(id);
    if (vStreetData.isPresent()) {
      try {
        gIStreetRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Street vStreetNull = new Street();
      return new ResponseEntity<>(vStreetNull, HttpStatus.NOT_FOUND);
    }
  }
}
