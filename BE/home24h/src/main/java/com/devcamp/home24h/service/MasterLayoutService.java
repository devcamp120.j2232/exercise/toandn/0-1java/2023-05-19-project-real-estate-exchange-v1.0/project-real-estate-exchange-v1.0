package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class MasterLayoutService {
  @Autowired
  IMasterLayoutRepositoy gIMasterLayoutRepository;

  public ArrayList<MasterLayout> getAllMasterLayout() {
    ArrayList<MasterLayout> listMasterLayout = new ArrayList<>();
    gIMasterLayoutRepository.findAll().forEach(listMasterLayout::add);
    return listMasterLayout;
  }

  public MasterLayout createMasterLayout(MasterLayout pMasterLayout,
      Optional<Project> pProjectData) {
    try {
      MasterLayout vMasterLayout = new MasterLayout();
      vMasterLayout.setAcreage(pMasterLayout.getAcreage());
      vMasterLayout.setApartmentList(pMasterLayout.getApartmentList());
      vMasterLayout.setDateCreate(pMasterLayout.getDateCreate());
      vMasterLayout.setDescription(pMasterLayout.getDescription());
      vMasterLayout.setName(pMasterLayout.getName());
      vMasterLayout.setPhoto(pMasterLayout.getPhoto());
      vMasterLayout.setProject(pProjectData.get());
      MasterLayout vMasterLayoutSave = gIMasterLayoutRepository.save(vMasterLayout);
      return vMasterLayoutSave;
    } catch (Exception e) {
      return null;
    }
  }

  public MasterLayout updateMasterLayout(MasterLayout pMasterLayout, Optional<MasterLayout> pMasterLayoutData,
      Optional<Project> pProjectData) {
    try {
      MasterLayout vMasterLayout = new MasterLayout();
      vMasterLayout.setAcreage(pMasterLayout.getAcreage());
      vMasterLayout.setApartmentList(pMasterLayout.getApartmentList());
      vMasterLayout.setDateCreate(pMasterLayout.getDateCreate());
      vMasterLayout.setDescription(pMasterLayout.getDescription());
      vMasterLayout.setName(pMasterLayout.getName());
      vMasterLayout.setPhoto(pMasterLayout.getPhoto());
      vMasterLayout.setProject(pProjectData.get());
      MasterLayout vMasterLayoutSave = gIMasterLayoutRepository.save(vMasterLayout);
      return vMasterLayoutSave;
    } catch (Exception e) {
      return null;
    }
  }
}
