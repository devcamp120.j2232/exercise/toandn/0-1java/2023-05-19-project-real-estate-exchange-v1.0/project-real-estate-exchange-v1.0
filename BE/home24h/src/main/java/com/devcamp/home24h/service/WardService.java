package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class WardService {
  @Autowired
  IWardRepository gIWardRepository;

  public ArrayList<Ward> getAllWard() {
    ArrayList<Ward> listWard = new ArrayList<>();
    gIWardRepository.findAll().forEach(listWard::add);
    return listWard;
  }

  public Ward createWard(Ward pWard,
      Optional<District> pDistrictData, Optional<Province> pProvinceData) {
    try {
      Ward vWard = new Ward();
      vWard.setName(pWard.getName());
      vWard.setPrefix(pWard.getPrefix());
      vWard.setProvince(pProvinceData.get());
      vWard.setDistrict(pDistrictData.get());
      Ward vWardSave = gIWardRepository.save(vWard);
      return vWardSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Ward updateWard(Ward pWard, Optional<Ward> pWardData,
      Optional<District> pDistrictData, Optional<Province> pProvinceData) {
    try {
      Ward vWard = pWardData.get();
      vWard.setName(pWard.getName());
      vWard.setPrefix(pWard.getPrefix());
      vWard.setProvince(pProvinceData.get());
      vWard.setDistrict(pDistrictData.get());
      Ward vWardSave = gIWardRepository.save(vWard);
      return vWardSave;
    } catch (Exception e) {
      return null;
    }
  }
}
