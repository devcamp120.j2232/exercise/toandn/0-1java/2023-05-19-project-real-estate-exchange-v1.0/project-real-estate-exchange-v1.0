package com.devcamp.home24h.controller;

import java.util.*;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/realestate")
public class RealeStateController {
  @Autowired
  IRealeStateRepository gIRealeStateRepository;
  @Autowired
  IStreetRepository gIStreetRepository;
  @Autowired
  IWardRepository gIWardRepository;
  @Autowired
  IDistrictRepository gIDistrictRepository;
  @Autowired
  IProvinceRepository gIProvinceRepository;
  @Autowired
  IProjectRepository gIProjectRepository;
  @Autowired
  ICustomerRepository gICustomerRepository;
  @Autowired
  RealeStateService gRealeStateService;

  @GetMapping("/all")
  public ResponseEntity<List<RealeState>> getAllRealeState() {
    try {
      return new ResponseEntity<>(gRealeStateService.getAllRealeState(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{StreetId}/{WardId}/{DistrictId}/{ProvinceId}/{ProjectId}/{CustomerId}")
  public ResponseEntity<Object> createRealeState(@PathVariable Integer StreetId, @PathVariable Integer WardId,
      @PathVariable Integer DistrictId, @PathVariable Integer ProvinceId, @PathVariable Integer ProjectId,
      @PathVariable Integer CustomerId,
      @Valid @RequestBody RealeState paramRealeState) {
    Optional<Street> vStreetData = gIStreetRepository.findById(StreetId);
    Optional<Ward> vWardData = gIWardRepository.findById(WardId);
    Optional<District> vDistrictData = gIDistrictRepository.findById(DistrictId);
    Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
    Optional<Project> vProjectData = gIProjectRepository.findById(ProjectId);
    Optional<Customer> vCustomerData = gICustomerRepository.findById(CustomerId);
    if (vStreetData.isPresent() && vWardData.isPresent() && vDistrictData.isPresent() && vProvinceData.isPresent()
        && vProjectData.isPresent() && vCustomerData.isPresent()) {
      try {
        return new ResponseEntity<>(
            gRealeStateService.createRealeState(paramRealeState, vStreetData, vWardData, vDistrictData, vProvinceData,
                vProjectData, vCustomerData),
            HttpStatus.CREATED);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Create specified RealeState: " + e.getCause().getCause().getMessage());
      }
    } else {
      District vDistrictNull = new District();
      return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/update/{id}/{StreetId}/{WardId}/{DistrictId}/{ProvinceId}/{ProjectId}/{CustomerId}")
  public ResponseEntity<Object> updateRealeState(@PathVariable Integer StreetId, @PathVariable Integer WardId,
      @PathVariable Integer id, @PathVariable Integer DistrictId,
      @PathVariable Integer ProvinceId, @PathVariable Integer ProjectId,
      @PathVariable Integer CustomerId,
      @Valid @RequestBody RealeState paramRealeState) {
    Optional<RealeState> vRealeStateData = gIRealeStateRepository.findById(id);
    if (vRealeStateData.isPresent()) {
      Optional<Street> vStreetData = gIStreetRepository.findById(StreetId);
      Optional<Ward> vWardData = gIWardRepository.findById(WardId);
      Optional<District> vDistrictData = gIDistrictRepository.findById(DistrictId);
      Optional<Province> vProvinceData = gIProvinceRepository.findById(ProvinceId);
      Optional<Project> vProjectData = gIProjectRepository.findById(ProjectId);
      Optional<Customer> vCustomerData = gICustomerRepository.findById(CustomerId);
      if (vStreetData.isPresent() && vWardData.isPresent() && vDistrictData.isPresent() && vProvinceData.isPresent()
          && vProjectData.isPresent() && vCustomerData.isPresent()) {
        try {
          return new ResponseEntity<>(
              gRealeStateService.updateRealeState(paramRealeState, vRealeStateData, vStreetData, vWardData,
                  vDistrictData,
                  vProvinceData,
                  vProjectData, vCustomerData),
              HttpStatus.OK);
        } catch (Exception e) {
          return ResponseEntity.unprocessableEntity()
              .body("Failed to Update specified RealeState: " + e.getCause().getCause().getMessage());
        }
      } else {
        District vDistrictNull = new District();
        return new ResponseEntity<>(vDistrictNull, HttpStatus.NOT_FOUND);
      }
    } else {
      RealeState vRealeStateNull = new RealeState();
      return new ResponseEntity<>(vRealeStateNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteRealeStateById(@PathVariable Integer id) {
    Optional<RealeState> vRealeStateData = gIRealeStateRepository.findById(id);
    if (vRealeStateData.isPresent()) {
      try {
        gIRealeStateRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      RealeState vRealeStateNull = new RealeState();
      return new ResponseEntity<>(vRealeStateNull, HttpStatus.NOT_FOUND);
    }
  }
}
