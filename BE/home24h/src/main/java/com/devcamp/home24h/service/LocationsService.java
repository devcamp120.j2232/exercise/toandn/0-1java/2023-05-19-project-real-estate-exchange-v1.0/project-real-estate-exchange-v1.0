package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.Locations;
import com.devcamp.home24h.repository.ILocationsRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class LocationsService {
  @Autowired
  ILocationsRepository gILocationsRepository;

  public ArrayList<Locations> getAllLocations() {
    ArrayList<Locations> listLocations = new ArrayList<>();
    gILocationsRepository.findAll().forEach(listLocations::add);
    return listLocations;
  }

  public Locations createLocations(Locations pLocations) {
    try {
      Locations vLocationsSave = gILocationsRepository.save(pLocations);
      return vLocationsSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Locations updateLocations(Locations pLocations, Optional<Locations> pLocationsData) {
    try {
      Locations vLocations = pLocationsData.get();
      vLocations.setLatitude(pLocations.getLatitude());
      vLocations.setLongitude(pLocations.getLongitude());
      Locations vLocationsSave = gILocationsRepository.save(vLocations);
      return vLocationsSave;
    } catch (Exception e) {
      return null;
    }
  }
}
