package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.Employees;
import com.devcamp.home24h.repository.IEmployeesRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class EmployeesService {
  @Autowired
  IEmployeesRepository gIEmployeesRepository;

  public ArrayList<Employees> getAllEmployees() {
    ArrayList<Employees> listEmployees = new ArrayList<>();
    gIEmployeesRepository.findAll().forEach(listEmployees::add);
    return listEmployees;
  }

  public Employees createEmployees(Employees pEmployees) {
    try {
      Employees vEmployeesSave = gIEmployeesRepository.save(pEmployees);
      return vEmployeesSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Employees updateEmployees(Employees pEmployees, Optional<Employees> pEmployeesData) {
    try {
      Employees vEmployees = pEmployeesData.get();
      vEmployees.setAddress(pEmployees.getAddress());
      vEmployees.setBirthDate(pEmployees.getBirthDate());
      vEmployees.setCity(pEmployees.getCity());
      vEmployees.setCountry(pEmployees.getCountry());
      vEmployees.setEmail(pEmployees.getEmail());
      vEmployees.setExtension(pEmployees.getExtension());
      vEmployees.setFirstName(pEmployees.getFirstName());
      vEmployees.setHireDate(pEmployees.getHireDate());
      vEmployees.setHomePhone(pEmployees.getHomePhone());
      vEmployees.setLastName(pEmployees.getLastName());
      vEmployees.setNotes(pEmployees.getNotes());
      vEmployees.setPassword(pEmployees.getPassword());
      vEmployees.setPhoto(pEmployees.getPhoto());
      vEmployees.setPostalCode(pEmployees.getPostalCode());
      vEmployees.setProfile(pEmployees.getProfile());
      vEmployees.setRegion(pEmployees.getRegion());
      vEmployees.setReportsTo(pEmployees.getReportsTo());
      vEmployees.setTitle(pEmployees.getTitle());
      vEmployees.setTitleOfCourtesy(pEmployees.getTitleOfCourtesy());
      vEmployees.setUserLevel(pEmployees.getUserLevel());
      vEmployees.setUsername(pEmployees.getUsername());
      Employees vEmployeesSave = gIEmployeesRepository.save(vEmployees);
      return vEmployeesSave;
    } catch (Exception e) {
      return null;
    }
  }
}
