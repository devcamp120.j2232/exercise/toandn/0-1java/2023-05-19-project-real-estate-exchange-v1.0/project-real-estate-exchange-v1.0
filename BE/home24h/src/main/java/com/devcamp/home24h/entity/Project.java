package com.devcamp.home24h.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "project")
public class Project {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "_name", length = 200)
  private String name;

  @Column(name = "address", length = 1000)
  private String address;

  @Column(name = "slogan", columnDefinition = "MEDIUMTEXT")
  private String slogan;

  @Column(name = "description", columnDefinition = "TEXT")
  private String description;

  @Column(name = "acreage")
  private BigDecimal acreage;

  @Column(name = "construct_area")
  private BigDecimal constructArea;

  @Column(name = "num_block")
  private Short numBlock;

  @Column(name = "num_floors", length = 500)
  private String numFloors;

  @Column(name = "num_apartment")
  private Integer numApartment;

  @Column(name = "apartment_area", length = 500)
  private String apartmentArea;

  @Column(name = "investor")
  private Integer investor;

  @Column(name = "construction_contractor")
  private Integer constructionContractor;

  @Column(name = "design_unit")
  private Integer designUnit;

  @Column(name = "utilities", length = 1000, nullable = false)
  private String utilities;

  @Column(name = "region_link", length = 1000, nullable = false)
  private String regionLink;

  @Column(name = "photo", length = 5000)
  private String photo;

  @Column(name = "_lat")
  private Double lat;

  @Column(name = "_lng")
  private Double lng;

  @OneToMany(mappedBy = "project")
  private List<MasterLayout> masterLayouts;

  @ManyToOne
  @JsonIgnore
  private Province province;

  @ManyToOne
  @JsonIgnore
  private District district;

  @ManyToOne
  @JsonIgnore
  private Ward ward;

  @ManyToOne
  @JsonIgnore
  private Street street;

  public Project() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getSlogan() {
    return slogan;
  }

  public void setSlogan(String slogan) {
    this.slogan = slogan;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public BigDecimal getAcreage() {
    return acreage;
  }

  public void setAcreage(BigDecimal acreage) {
    this.acreage = acreage;
  }

  public BigDecimal getConstructArea() {
    return constructArea;
  }

  public void setConstructArea(BigDecimal constructArea) {
    this.constructArea = constructArea;
  }

  public Short getNumBlock() {
    return numBlock;
  }

  public void setNumBlock(Short numBlock) {
    this.numBlock = numBlock;
  }

  public String getNumFloors() {
    return numFloors;
  }

  public void setNumFloors(String numFloors) {
    this.numFloors = numFloors;
  }

  public Integer getNumApartment() {
    return numApartment;
  }

  public void setNumApartment(Integer numApartment) {
    this.numApartment = numApartment;
  }

  public String getApartmentArea() {
    return apartmentArea;
  }

  public void setApartmentArea(String apartmentArea) {
    this.apartmentArea = apartmentArea;
  }

  public Integer getInvestor() {
    return investor;
  }

  public void setInvestor(Integer investor) {
    this.investor = investor;
  }

  public Integer getConstructionContractor() {
    return constructionContractor;
  }

  public void setConstructionContractor(Integer constructionContractor) {
    this.constructionContractor = constructionContractor;
  }

  public Integer getDesignUnit() {
    return designUnit;
  }

  public void setDesignUnit(Integer designUnit) {
    this.designUnit = designUnit;
  }

  public String getUtilities() {
    return utilities;
  }

  public void setUtilities(String utilities) {
    this.utilities = utilities;
  }

  public String getRegionLink() {
    return regionLink;
  }

  public void setRegionLink(String regionLink) {
    this.regionLink = regionLink;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public Double getLng() {
    return lng;
  }

  public void setLng(Double lng) {
    this.lng = lng;
  }

  public List<MasterLayout> getMasterLayouts() {
    return masterLayouts;
  }

  public void setMasterLayouts(List<MasterLayout> masterLayouts) {
    this.masterLayouts = masterLayouts;
  }

  public Province getProvince() {
    return province;
  }

  public void setProvince(Province province) {
    this.province = province;
  }

  public District getDistrict() {
    return district;
  }

  public void setDistrict(District district) {
    this.district = district;
  }

  public Ward getWard() {
    return ward;
  }

  public void setWard(Ward ward) {
    this.ward = ward;
  }

  public Street getStreet() {
    return street;
  }

  public void setStreet(Street street) {
    this.street = street;
  }

}
