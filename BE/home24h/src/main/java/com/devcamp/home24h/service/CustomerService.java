package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.Customer;
import com.devcamp.home24h.repository.ICustomerRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
  @Autowired
  ICustomerRepository gICustomerRepository;

  public ArrayList<Customer> getAllCustomer() {
    ArrayList<Customer> listCustomer = new ArrayList<>();
    gICustomerRepository.findAll().forEach(listCustomer::add);
    return listCustomer;
  }

  public Customer createCustomer(Customer pCustomer) {
    try {
      Customer vCustomerSave = gICustomerRepository.save(pCustomer);
      return vCustomerSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Customer updateCustomer(Customer pCustomer, Optional<Customer> pCustomerData) {
    try {
      Customer vCustomer = pCustomerData.get();
      vCustomer.setAddress(pCustomer.getAddress());
      vCustomer.setContactName(pCustomer.getContactName());
      vCustomer.setContactTitle(pCustomer.getContactTitle());
      vCustomer.setCreateBy(pCustomer.getCreateBy());
      vCustomer.setCreateDate(pCustomer.getCreateDate());
      vCustomer.setEmail(pCustomer.getEmail());
      vCustomer.setMobile(pCustomer.getMobile());
      vCustomer.setNote(pCustomer.getNote());
      vCustomer.setRealeStates(pCustomer.getRealeStates());
      vCustomer.setUpdateDate(pCustomer.getUpdateDate());
      vCustomer.setUpdate_by(pCustomer.getUpdate_by());
      Customer vCustomerSave = gICustomerRepository.save(vCustomer);
      return vCustomerSave;
    } catch (Exception e) {
      return null;
    }
  }
}
