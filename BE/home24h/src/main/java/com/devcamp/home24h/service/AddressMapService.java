package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.AddressMap;
import com.devcamp.home24h.repository.IAddressMapRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class AddressMapService {
  @Autowired
  IAddressMapRepository gIAddressMapRepository;

  public ArrayList<AddressMap> getAllAddressMap() {
    ArrayList<AddressMap> listAddressMap = new ArrayList<>();
    gIAddressMapRepository.findAll().forEach(listAddressMap::add);
    return listAddressMap;
  }

  public AddressMap createAddressMap(AddressMap pAddressMap) {
    try {
      AddressMap vAddressMapSave = gIAddressMapRepository.save(pAddressMap);
      return vAddressMapSave;
    } catch (Exception e) {
      return null;
    }
  }

  public AddressMap updateAddressMap(AddressMap pAddressMap, Optional<AddressMap> pAddressMapData) {
    try {
      AddressMap vAddressMap = pAddressMapData.get();
      vAddressMap.setAddress(pAddressMap.getAddress());
      vAddressMap.setLatitude(pAddressMap.getLatitude());
      vAddressMap.setLongitude(pAddressMap.getLongitude());
      AddressMap vAddressMapSave = gIAddressMapRepository.save(vAddressMap);
      return vAddressMapSave;
    } catch (Exception e) {
      return null;
    }
  }
}
