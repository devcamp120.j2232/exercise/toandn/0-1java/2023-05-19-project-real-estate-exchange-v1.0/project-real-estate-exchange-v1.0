package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.RegionLink;
import com.devcamp.home24h.repository.IRegionLinkRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class RegionLinkService {
  @Autowired
  IRegionLinkRepository gIRegionLinkRepository;

  public ArrayList<RegionLink> getAllRegionLink() {
    ArrayList<RegionLink> listRegionLink = new ArrayList<>();
    gIRegionLinkRepository.findAll().forEach(listRegionLink::add);
    return listRegionLink;
  }

  public RegionLink createRegionLink(RegionLink pRegionLink) {
    try {
      RegionLink vRegionLinkSave = gIRegionLinkRepository.save(pRegionLink);
      return vRegionLinkSave;
    } catch (Exception e) {
      return null;
    }
  }

  public RegionLink updateRegionLink(RegionLink pRegionLink, Optional<RegionLink> pRegionLinkData) {
    try {
      RegionLink vRegionLink = pRegionLinkData.get();
      vRegionLink.setAddress(pRegionLink.getAddress());
      vRegionLink.setDescription(pRegionLink.getDescription());
      vRegionLink.setLat(pRegionLink.getLat());
      vRegionLink.setLng(pRegionLink.getLng());
      vRegionLink.setName(pRegionLink.getName());
      vRegionLink.setPhoto(pRegionLink.getPhoto());
      RegionLink vRegionLinkSave = gIRegionLinkRepository.save(vRegionLink);
      return vRegionLinkSave;
    } catch (Exception e) {
      return null;
    }
  }
}
