package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.Subscriptions;
import com.devcamp.home24h.repository.ISubscriptionsRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionsService {
  @Autowired
  ISubscriptionsRepository gISubscriptionsRepository;

  public ArrayList<Subscriptions> getAllSubscriptions() {
    ArrayList<Subscriptions> listSubscriptions = new ArrayList<>();
    gISubscriptionsRepository.findAll().forEach(listSubscriptions::add);
    return listSubscriptions;
  }

  public Subscriptions createSubscriptions(Subscriptions pSubscriptions) {
    try {
      Subscriptions vSubscriptionsSave = gISubscriptionsRepository.save(pSubscriptions);
      return vSubscriptionsSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Subscriptions updateSubscriptions(Subscriptions pSubscriptions, Optional<Subscriptions> pSubscriptionsData) {
    try {
      Subscriptions vSubscriptions = pSubscriptionsData.get();
      vSubscriptions.setAuthenticationToken(pSubscriptions.getAuthenticationToken());
      vSubscriptions.setContentEncoding(pSubscriptions.getContentEncoding());
      vSubscriptions.setEndpoint(pSubscriptions.getEndpoint());
      vSubscriptions.setPublicKey(pSubscriptions.getPublicKey());
      vSubscriptions.setUser(pSubscriptions.getUser());
      Subscriptions vSubscriptionsSave = gISubscriptionsRepository.save(vSubscriptions);
      return vSubscriptionsSave;
    } catch (Exception e) {
      return null;
    }
  }
}
