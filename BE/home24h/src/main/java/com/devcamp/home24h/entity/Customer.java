package com.devcamp.home24h.entity;

import java.util.List;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "customers")
public class Customer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "contact_name", length = 255)
  private String contactName;

  @Column(name = "contact_title", length = 30)
  private String contactTitle;

  @Column(name = "address", length = 200)
  private String address;

  @Column(name = "mobile", length = 80)
  private String mobile;

  @Column(name = "email", length = 24)
  private String email;

  @Column(name = "note", length = 5000)
  private String note;

  @Column(name = "create_by")
  private Integer createBy;

  @Column(name = "update_by")
  private Integer update_by;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_date")
  private Date createDate;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "update_date")
  private Date updateDate;

  @OneToMany(mappedBy = "customer")
  private List<RealeState> realeStates;

  public Customer() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getContactName() {
    return contactName;
  }

  public void setContactName(String contactName) {
    this.contactName = contactName;
  }

  public String getContactTitle() {
    return contactTitle;
  }

  public void setContactTitle(String contactTitle) {
    this.contactTitle = contactTitle;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public Integer getCreateBy() {
    return createBy;
  }

  public void setCreateBy(Integer createBy) {
    this.createBy = createBy;
  }

  public Integer getUpdate_by() {
    return update_by;
  }

  public void setUpdate_by(Integer update_by) {
    this.update_by = update_by;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public List<RealeState> getRealeStates() {
    return realeStates;
  }

  public void setRealeStates(List<RealeState> realeStates) {
    this.realeStates = realeStates;
  }

}
