package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/province")
public class ProvinceController {
  @Autowired
  IProvinceRepository gIProvinceRepository;
  @Autowired
  ProvinceService gProvinceService;

  @GetMapping("/all")
  public ResponseEntity<List<Province>> getAllProvince() {
    try {
      return new ResponseEntity<>(gProvinceService.getAllProvince(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/details/{id}")
  public ResponseEntity<Object> getProvinceById(@PathVariable Integer id) {
    Optional<Province> vProvinceData = gIProvinceRepository.findById(id);
    if (vProvinceData.isPresent()) {
      try {
        Province vProvince = vProvinceData.get();
        return new ResponseEntity<>(vProvince, HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Province vProvinceNull = new Province();
      return new ResponseEntity<>(vProvinceNull, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createProvince(@Valid @RequestBody Province paramProvince) {
    try {
      return new ResponseEntity<>(gProvinceService.createProvince(paramProvince), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Province: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateProvince(@PathVariable Integer id,
      @Valid @RequestBody Province paramProvince) {
    Optional<Province> vProvinceData = gIProvinceRepository.findById(id);
    if (vProvinceData.isPresent()) {
      try {
        return new ResponseEntity<>(gProvinceService.updateProvince(paramProvince, vProvinceData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Province: " + e.getCause().getCause().getMessage());
      }
    } else {
      Province vProvinceNull = new Province();
      return new ResponseEntity<>(vProvinceNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<Province> vProvinceData = gIProvinceRepository.findById(id);
    if (vProvinceData.isPresent()) {
      try {
        gIProvinceRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Province vProvinceNull = new Province();
      return new ResponseEntity<>(vProvinceNull, HttpStatus.NOT_FOUND);
    }
  }
}
