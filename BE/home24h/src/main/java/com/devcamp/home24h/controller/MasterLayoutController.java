package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/masterLayout")
public class MasterLayoutController {
  @Autowired
  IMasterLayoutRepositoy gIMasterLayoutRepository;
  @Autowired
  IProjectRepository gIProjectRepository;
  @Autowired
  MasterLayoutService gMasterLayoutService;

  @GetMapping("/all")
  public ResponseEntity<List<MasterLayout>> getAllMasterLayout() {
    try {
      return new ResponseEntity<>(gMasterLayoutService.getAllMasterLayout(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{ProjectId}")
  public ResponseEntity<Object> createMasterLayout(@PathVariable Integer ProjectId,
      @Valid @RequestBody MasterLayout paramMasterLayout) {
    Optional<Project> vProjectData = gIProjectRepository.findById(ProjectId);
    if (vProjectData.isPresent()) {
      try {
        return new ResponseEntity<>(gMasterLayoutService.createMasterLayout(paramMasterLayout, vProjectData),
            HttpStatus.CREATED);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Create specified MasterLayout: " + e.getCause().getCause().getMessage());
      }
    } else {
      Project vProjectNull = new Project();
      return new ResponseEntity<>(vProjectNull, HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/update/{id}/{ProjectId}")
  public ResponseEntity<Object> updateMasterLayout(@PathVariable Integer id, @PathVariable Integer ProjectId,
      @Valid @RequestBody MasterLayout paramMasterLayout) {
    Optional<MasterLayout> vMasterLayoutData = gIMasterLayoutRepository.findById(id);
    if (vMasterLayoutData.isPresent()) {
      Optional<Project> vProjectData = gIProjectRepository.findById(ProjectId);
      if (vProjectData.isPresent()) {
        try {
          return new ResponseEntity<>(
              gMasterLayoutService.updateMasterLayout(paramMasterLayout, vMasterLayoutData, vProjectData),
              HttpStatus.OK);
        } catch (Exception e) {
          return ResponseEntity.unprocessableEntity()
              .body("Failed to Update specified MasterLayout: " + e.getCause().getCause().getMessage());
        }
      } else {
        Project vProjectNull = new Project();
        return new ResponseEntity<>(vProjectNull, HttpStatus.NOT_FOUND);
      }
    } else {
      MasterLayout vMasterLayoutNull = new MasterLayout();
      return new ResponseEntity<>(vMasterLayoutNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteMasterLayoutById(@PathVariable Integer id) {
    Optional<MasterLayout> vMasterLayoutData = gIMasterLayoutRepository.findById(id);
    if (vMasterLayoutData.isPresent()) {
      try {
        gIMasterLayoutRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      MasterLayout vMasterLayoutNull = new MasterLayout();
      return new ResponseEntity<>(vMasterLayoutNull, HttpStatus.NOT_FOUND);
    }
  }
}
