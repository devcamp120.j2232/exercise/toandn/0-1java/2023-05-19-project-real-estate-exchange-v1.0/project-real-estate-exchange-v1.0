package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/employees")
public class EmployeesController {
  @Autowired
  IEmployeesRepository gIEmployeesRepository;
  @Autowired
  EmployeesService gEmployeesService;

  @GetMapping("/all")
  public ResponseEntity<List<Employees>> getAllEmployees() {
    try {
      return new ResponseEntity<>(gEmployeesService.getAllEmployees(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/details/{id}")
  public ResponseEntity<Object> getEmployeesById(@PathVariable Integer id) {
    Optional<Employees> vEmployeesData = gIEmployeesRepository.findById(id);
    if (vEmployeesData.isPresent()) {
      try {
        Employees vEmployees = vEmployeesData.get();
        return new ResponseEntity<>(vEmployees, HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Employees vEmployeesNull = new Employees();
      return new ResponseEntity<>(vEmployeesNull, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createEmployees(@Valid @RequestBody Employees paramEmployees) {
    try {
      return new ResponseEntity<>(gEmployeesService.createEmployees(paramEmployees), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Employees: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateEmployees(@PathVariable Integer id,
      @Valid @RequestBody Employees paramEmployees) {
    Optional<Employees> vEmployeesData = gIEmployeesRepository.findById(id);
    if (vEmployeesData.isPresent()) {
      try {
        return new ResponseEntity<>(gEmployeesService.updateEmployees(paramEmployees, vEmployeesData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Employees: " + e.getCause().getCause().getMessage());
      }
    } else {
      Employees vEmployeesNull = new Employees();
      return new ResponseEntity<>(vEmployeesNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<Employees> vEmployeesData = gIEmployeesRepository.findById(id);
    if (vEmployeesData.isPresent()) {
      try {
        gIEmployeesRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Employees vEmployeesNull = new Employees();
      return new ResponseEntity<>(vEmployeesNull, HttpStatus.NOT_FOUND);
    }
  }
}
