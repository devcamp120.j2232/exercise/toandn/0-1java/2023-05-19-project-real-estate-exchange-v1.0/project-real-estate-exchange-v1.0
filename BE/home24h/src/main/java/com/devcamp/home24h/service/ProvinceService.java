package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.Province;
import com.devcamp.home24h.repository.IProvinceRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class ProvinceService {
  @Autowired
  IProvinceRepository gIProvinceRepository;

  public ArrayList<Province> getAllProvince() {
    ArrayList<Province> listProvince = new ArrayList<>();
    gIProvinceRepository.findAll().forEach(listProvince::add);
    return listProvince;
  }

  public Province createProvince(Province pProvince) {
    try {
      Province vProvinceSave = gIProvinceRepository.save(pProvince);
      return vProvinceSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Province updateProvince(Province pProvince, Optional<Province> pProvinceData) {
    try {
      Province vProvince = pProvinceData.get();
      vProvince.setCode(pProvince.getCode());
      vProvince.setDistricts(pProvince.getDistricts());
      vProvince.setName(pProvince.getName());
      vProvince.setProjects(pProvince.getProjects());
      vProvince.setRealeStates(pProvince.getRealeStates());
      vProvince.setStreets(pProvince.getStreets());
      vProvince.setWards(pProvince.getWards());
      Province vProvinceSave = gIProvinceRepository.save(vProvince);
      return vProvinceSave;
    } catch (Exception e) {
      return null;
    }
  }
}
