package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/regionLink")
public class RegionLinkController {
  @Autowired
  IRegionLinkRepository gIRegionLinkRepository;
  @Autowired
  RegionLinkService gRegionLinkService;

  @GetMapping("/all")
  public ResponseEntity<List<RegionLink>> getAllRegionLink() {
    try {
      return new ResponseEntity<>(gRegionLinkService.getAllRegionLink(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/details/{id}")
  public ResponseEntity<Object> getRegionLinkById(@PathVariable Integer id) {
    Optional<RegionLink> vRegionLinkData = gIRegionLinkRepository.findById(id);
    if (vRegionLinkData.isPresent()) {
      try {
        RegionLink vRegionLink = vRegionLinkData.get();
        return new ResponseEntity<>(vRegionLink, HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      RegionLink vRegionLinkNull = new RegionLink();
      return new ResponseEntity<>(vRegionLinkNull, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createRegionLink(@Valid @RequestBody RegionLink paramRegionLink) {
    try {
      return new ResponseEntity<>(gRegionLinkService.createRegionLink(paramRegionLink), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified RegionLink: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateRegionLink(@PathVariable Integer id,
      @Valid @RequestBody RegionLink paramRegionLink) {
    Optional<RegionLink> vRegionLinkData = gIRegionLinkRepository.findById(id);
    if (vRegionLinkData.isPresent()) {
      try {
        return new ResponseEntity<>(gRegionLinkService.updateRegionLink(paramRegionLink, vRegionLinkData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified RegionLink: " + e.getCause().getCause().getMessage());
      }
    } else {
      RegionLink vRegionLinkNull = new RegionLink();
      return new ResponseEntity<>(vRegionLinkNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<RegionLink> vRegionLinkData = gIRegionLinkRepository.findById(id);
    if (vRegionLinkData.isPresent()) {
      try {
        gIRegionLinkRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      RegionLink vRegionLinkNull = new RegionLink();
      return new ResponseEntity<>(vRegionLinkNull, HttpStatus.NOT_FOUND);
    }
  }
}
