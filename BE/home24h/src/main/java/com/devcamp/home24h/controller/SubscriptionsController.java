package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/Subscriptions")
public class SubscriptionsController {
  @Autowired
  ISubscriptionsRepository gISubscriptionsRepository;
  @Autowired
  SubscriptionsService gSubscriptionsService;

  @GetMapping("/all")
  public ResponseEntity<List<Subscriptions>> getAllSubscriptions() {
    try {
      return new ResponseEntity<>(gSubscriptionsService.getAllSubscriptions(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/details/{id}")
  public ResponseEntity<Object> getSubscriptionsById(@PathVariable Integer id) {
    Optional<Subscriptions> vSubscriptionsData = gISubscriptionsRepository.findById(id);
    if (vSubscriptionsData.isPresent()) {
      try {
        Subscriptions vSubscriptions = vSubscriptionsData.get();
        return new ResponseEntity<>(vSubscriptions, HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Subscriptions vSubscriptionsNull = new Subscriptions();
      return new ResponseEntity<>(vSubscriptionsNull, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createSubscriptions(@Valid @RequestBody Subscriptions paramSubscriptions) {
    try {
      return new ResponseEntity<>(gSubscriptionsService.createSubscriptions(paramSubscriptions), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Subscriptions: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateSubscriptions(@PathVariable Integer id,
      @Valid @RequestBody Subscriptions paramSubscriptions) {
    Optional<Subscriptions> vSubscriptionsData = gISubscriptionsRepository.findById(id);
    if (vSubscriptionsData.isPresent()) {
      try {
        return new ResponseEntity<>(gSubscriptionsService.updateSubscriptions(paramSubscriptions, vSubscriptionsData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Subscriptions: " + e.getCause().getCause().getMessage());
      }
    } else {
      Subscriptions vSubscriptionsNull = new Subscriptions();
      return new ResponseEntity<>(vSubscriptionsNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<Subscriptions> vSubscriptionsData = gISubscriptionsRepository.findById(id);
    if (vSubscriptionsData.isPresent()) {
      try {
        gISubscriptionsRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Subscriptions vSubscriptionsNull = new Subscriptions();
      return new ResponseEntity<>(vSubscriptionsNull, HttpStatus.NOT_FOUND);
    }
  }
}
