package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/customer")
public class CustomerController {
  @Autowired
  ICustomerRepository gICustomerRepository;
  @Autowired
  CustomerService gCustomerService;

  @GetMapping("/all")
  public ResponseEntity<List<Customer>> getAllCustomer() {
    try {
      return new ResponseEntity<>(gCustomerService.getAllCustomer(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/details/{id}")
  public ResponseEntity<Object> getCustomerById(@PathVariable Integer id) {
    Optional<Customer> vCustomerData = gICustomerRepository.findById(id);
    if (vCustomerData.isPresent()) {
      try {
        Customer vCustomer = vCustomerData.get();
        return new ResponseEntity<>(vCustomer, HttpStatus.OK);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Customer vCustomerNull = new Customer();
      return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customer paramCustomer) {
    try {
      return new ResponseEntity<>(gCustomerService.createCustomer(paramCustomer), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateCustomer(@PathVariable Integer id,
      @Valid @RequestBody Customer paramCustomer) {
    Optional<Customer> vCustomerData = gICustomerRepository.findById(id);
    if (vCustomerData.isPresent()) {
      try {
        return new ResponseEntity<>(gCustomerService.updateCustomer(paramCustomer, vCustomerData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Customer: " + e.getCause().getCause().getMessage());
      }
    } else {
      Customer vCustomerNull = new Customer();
      return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<Customer> vCustomerData = gICustomerRepository.findById(id);
    if (vCustomerData.isPresent()) {
      try {
        gICustomerRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Customer vCustomerNull = new Customer();
      return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
    }
  }
}
